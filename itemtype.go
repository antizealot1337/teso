package teso

import (
	"fmt"
	"strconv"
)

// ItemType represents a type of item in ESO.
type ItemType int

// Different item types.
const (
	ItemNone ItemType = iota
	ItemWeapon
	ItemArmor
	ItemPlug
	ItemFood
	ItemTrophy
	ItemSiege
	ItemPotion
	ItemRacialStyleMotif
	ItemTool
	ItemIngredient
	ItemAdditive
	ItemDrink
	ItemCostume
	ItemDisguise
	ItemTabard
	ItemLure
	ItemRawMaterial
	ItemContainer
	ItemSoulGem
	ItemGlyphWeapon
	ItemGlyphArmor
	ItemLockpick
	ItemWeaponBooster
	ItemArmorBooster
	ItemEnchatmentBooster
	ItemGlyphJewelry
	ItemSpice
	ItemFlavoring
	ItemRecipe
	ItemPoison
	ItemReagent
	itemDeprecated
	ItemPotionBase
	ItemCollectible
	ItemBlacksmithingRawMaterial
	ItemBlacksmithingMaterial
	ItemWoodworkingRawMaterial
	ItemWoodworkingMaterial
	ItemClothierRawMaterial
	ItemClothierMaterial
	ItemBlacksmithingBooster
	ItemWoodworkingBooster
	ItemClothierBooster
	ItemStyleMaterial
	ItemArmorTrait
	ItemWeaponTrait
	ItemAvaRepair
	ItemTrash
	ItemSpellcraftingTable
	ItemMount
	ItemEnchantingRunePotency
	ItemEnchantingRuneAspect
	ItemEnchantingRuneEssence
	ItemFish
	ItemCrownRepair
	ItemTreasure
	ItemCrownItem
	ItemPoisonBase
	ItemDyeStamp
	ItemWrit
	ItemFurniture
)

// String returns the string representation of an ItemType.
func (i ItemType) String() string {
	switch i {
	case ItemNone:
		return "None"
	case ItemWeapon:
		return "Weapon"
	case ItemArmor:
		return "Armor"
	case ItemPlug:
		return "Plug"
	case ItemFood:
		return "Food"
	case ItemTrophy:
		return "Trophy"
	case ItemSiege:
		return "Siege"
	case ItemPotion:
		return "Potion"
	case ItemRacialStyleMotif:
		return "Racial Style Motif"
	case ItemTool:
		return "Tool"
	case ItemIngredient:
		return "Ingredient"
	case ItemAdditive:
		return "Additive"
	case ItemDrink:
		return "Drink"
	case ItemCostume:
		return "Costume"
	case ItemDisguise:
		return "Disguise"
	case ItemTabard:
		return "Tabard"
	case ItemLure:
		return "Lure"
	case ItemRawMaterial:
		return "RawMaterial"
	case ItemContainer:
		return "Container"
	case ItemSoulGem:
		return "SoulGem"
	case ItemGlyphWeapon:
		return "Glyph Weapon"
	case ItemGlyphArmor:
		return "GlyphArmor"
	case ItemLockpick:
		return "Lockpick"
	case ItemWeaponBooster:
		return "Weapon Booster"
	case ItemArmorBooster:
		return "Armor Booster"
	case ItemEnchatmentBooster:
		return "Enchantment Booster"
	case ItemGlyphJewelry:
		return "Glyph Jewelry"
	case ItemSpice:
		return "Spice"
	case ItemFlavoring:
		return "Flavoring"
	case ItemRecipe:
		return "Recipe"
	case ItemPoison:
		return "Poison"
	case ItemReagent:
		return "Reagent"
	case itemDeprecated:
		return "Deprecated"
	case ItemPotionBase:
		return "Potion Base"
	case ItemCollectible:
		return "Collectible"
	case ItemBlacksmithingRawMaterial:
		return "Blacksmithing Raw Material"
	case ItemBlacksmithingMaterial:
		return "Blacksmithing Material"
	case ItemWoodworkingRawMaterial:
		return "Woodworking Raw Material"
	case ItemWoodworkingMaterial:
		return "Woodworking Material"
	case ItemClothierRawMaterial:
		return "Clothier Raw Material"
	case ItemClothierMaterial:
		return "Clothier Material"
	case ItemBlacksmithingBooster:
		return "Blacksmithing Booster"
	case ItemWoodworkingBooster:
		return "Woodworking Booster"
	case ItemClothierBooster:
		return "Clothier Booster"
	case ItemStyleMaterial:
		return "Style Material"
	case ItemArmorTrait:
		return "Armor Trait"
	case ItemWeaponTrait:
		return "Weapon Trait"
	case ItemAvaRepair:
		return "Ava Repair"
	case ItemTrash:
		return "Trash"
	case ItemSpellcraftingTable:
		return "Spellcrafting Table"
	case ItemMount:
		return "Mount"
	case ItemEnchantingRunePotency:
		return "Enchanting Rune Potency"
	case ItemEnchantingRuneAspect:
		return "Enchanting Rune Aspect"
	case ItemEnchantingRuneEssence:
		return "Enchanting Rune Essence"
	case ItemFish:
		return "Fish"
	case ItemCrownRepair:
		return "Crown Repair"
	case ItemTreasure:
		return "Treasure"
	case ItemCrownItem:
		return "Crown Item"
	case ItemPoisonBase:
		return "Poison Base"
	case ItemDyeStamp:
		return "Dye Stamp"
	case ItemWrit:
		return "Writ"
	case ItemFurniture:
		return "Furniture"
	default:
		return fmt.Sprintf("%d", i)
	} //switch
} //func

// UnmarshalText attempts to set the ItemType from the provided string.
func (i *ItemType) UnmarshalText(s string) error {
	t, err := strconv.Atoi(s)

	if err != nil {
		return err
	} //if

	*i = ItemType(t)

	return nil
} //func
