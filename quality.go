package teso

import (
	"fmt"
	"strconv"
)

// Quality is the quality of a piece of equipment.
type Quality int

// Different kinds of Quality.
const (
	QualityNormal Quality = iota
	QualityFine
	QualitySuperior
	QualityEpic
	QualityLegendary
)

// String is the string representation of a Quality.
func (q Quality) String() string {
	switch q {
	case QualityNormal:
		return "Normal"
	case QualityFine:
		return "Fine"
	case QualitySuperior:
		return "Superior"
	case QualityEpic:
		return "Epic"
	case QualityLegendary:
		return "Legendary"
	default:
		return fmt.Sprintf("%d", q)
	} //switc
} //func

func (q *Quality) UnmarshalText(s string) error {
	i, err := strconv.Atoi(s)

	if err != nil {
		return err
	} //if

	*q = Quality(i)

	return nil
} //func
