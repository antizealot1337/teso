package teso

import "strconv"

// EquipType represents the types of equipable items in ESO.
type EquipType int

// Different equipment types.
const (
	EquipNone EquipType = iota
	EquipHead
	EquipNeck
	EquipChest
	EquipShoulders
	EquipOneHand
	EquipTwoHand
	EquipOffHand
	EquipWaist
	EquipLegs
	EquipFeet
	EquipCostume
	EquipRing
	EquipHand
	EquipMainHand
	EquipPoison
)

// Strings is the string representation of the EquipType.
func (e EquipType) String() string {
	switch e {
	case EquipNone:
		return "None"
	case EquipHead:
		return "Head"
	case EquipNeck:
		return "Neck"
	case EquipChest:
		return "Chest"
	case EquipShoulders:
		return "Shoulders"
	case EquipOneHand:
		return "One Hand"
	case EquipTwoHand:
		return "Two Hand"
	case EquipOffHand:
		return "Off Hand"
	case EquipWaist:
		return "Waist"
	case EquipLegs:
		return "Legs"
	case EquipFeet:
		return "Feet"
	case EquipCostume:
		return "Costume"
	case EquipRing:
		return "Ring"
	case EquipHand:
		return "Hand"
	case EquipMainHand:
		return "Main Hand"
	case EquipPoison:
		return "Poison"
	default:
		return ""
	} //switch
} //func

// UnmarshalText attempts to set an EquipType from the provided string.
func (e *EquipType) UnmarshalText(s string) error {
	i, err := strconv.Atoi(s)

	if err != nil {
		return err
	} //if

	*e = EquipType(i)
	return nil
} //func
