package teso

import "errors"

// ArmorType is the weight of armor in ESO.
type ArmorType int

// Different armor types.
const (
	ArmorNone ArmorType = iota
	ArmorLight
	ArmorMedium
	ArmorHeavy
)

// String is the string representation of an ArmorType.
func (a ArmorType) String() string {
	switch a {
	case ArmorNone:
		return "None"
	case ArmorLight:
		return "Light"
	case ArmorMedium:
		return "Medium"
	case ArmorHeavy:
		return "Heavy"
	default:
		return ""
	} //switch
} //func

// UnmarshalText attempts to set an ArmorType using the provided string.
func (a *ArmorType) UnmarshalText(s string) error {
	switch s {
	case "0":
		*a = ArmorNone
	case "1":
		*a = ArmorLight
	case "2":
		*a = ArmorMedium
	case "3":
		*a = ArmorHeavy
	default:
		return errors.New("invalid armor type")
	} //switch

	return nil
} //func
