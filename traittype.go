package teso

import (
	"fmt"
	"strconv"
)

type TraitType int

const (
	TraitNone TraitType = iota
	TraitPowered
	TraitCharged
	TraitPrecise
	TraitWeaponInfused
	TraitDefending
	TraitWeaponTraining
	TraitSharpened
	TraitWeighted
	TraitWeaponIntricate
	TraitWeaponOrnate
	TraitSturdy
	TraitImpenatrable
	TraitReinforced
	TraitWellFitted
	TraitArmorTraining
	TraitArmorInfused
	TraitInvigorating
	TraitDivines
	TraitArmorOrnate
	TraitArmorIntricate
	TraitHealthy
	TraitArcane
	TraitRobust
	TraitJewelryOrnate
	TraitArmorNirnhoned
	TraitWeaponNirnhoned
	TraitSpecialStat
	TraitSwift
	TraitHarmony
	TraitTriune
	TraitBloodthirsty
)

func (t TraitType) String() string {
	switch t {
	case TraitNone:
		return "None"
	case TraitPowered:
		return "Powered"
	case TraitCharged:
		return "Charged"
	case TraitPrecise:
		return "Precise"
	case TraitWeaponInfused:
		return "Infused"
	case TraitDefending:
		return "Defending"
	case TraitWeaponTraining:
		return "Training"
	case TraitSharpened:
		return "Sharpened"
	case TraitWeighted:
		return "Weighted"
	case TraitWeaponIntricate:
		return "Intricate"
	case TraitWeaponOrnate:
		return "Ornate"
	case TraitSturdy:
		return "Sturdy"
	case TraitImpenatrable:
		return "Impenatrable"
	case TraitReinforced:
		return "Reinforced"
	case TraitWellFitted:
		return "Well Fitted"
	case TraitArmorTraining:
		return "Training"
	case TraitArmorInfused:
		return "Infused"
	case TraitInvigorating:
		return "Invigorating"
	case TraitDivines:
		return "Divines"
	case TraitArmorOrnate:
		return "Ornate"
	case TraitArmorIntricate:
		return "Intricate"
	case TraitHealthy:
		return "Healthy"
	case TraitArcane:
		return "Arcane"
	case TraitRobust:
		return "Robust"
	case TraitJewelryOrnate:
		return "Ornate"
	case TraitArmorNirnhoned:
		return "Nirnhoned"
	case TraitWeaponNirnhoned:
		return "Nirnhoned"
	case TraitSpecialStat:
		return "Special Stat"
	case TraitSwift:
		return "Swift"
	case TraitHarmony:
		return "Harmony"
	case TraitTriune:
		return "Triune"
	case TraitBloodthirsty:
		return "Bloodthirsty"
	default:
		return fmt.Sprintf("%d", t)
	} //switch
} //func

func (t *TraitType) UnmarshalTexxt(s string) error {
	i, err := strconv.Atoi(s)

	if err != nil {
		return err
	} //if

	*t = TraitType(i)

	return nil
} //func
