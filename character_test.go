package teso

import "testing"

func TestCharacterUnmarshalText(t *testing.T) {
	const line = `["8798292063127480"] = "CHARACTER:8798292063127480;Zair Rylanor;Sorcerer;2;3;0;Khajiit;9;3;71903000;1487849446;true-false-false;9608;1-1-1;EU Megaserver;@Heppy;114;140;en;34-60-0-60-0-60;0-0-0",`

	var c Character

	if err := c.UnmarshalText(line); err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if expected, actual := "8798292063127480", c.ID; actual != expected {
		t.Errorf("wrong id\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := "Zair Rylanor", c.Name; actual != expected {
		t.Errorf("wrong name\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := "Sorcerer", c.ClassID; actual != expected {
		t.Errorf("wrong class\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := AllianceDC, c.AllianceID; actual != expected {
		t.Errorf("expected alliance %v but was %v", expected, actual)
	} //if

	if expected, actual := "Khajiit", c.RaceID; actual != expected {
		t.Errorf("wrong class\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := 3, c.Level; actual != expected {
		t.Errorf("expected level %d but was %d", expected, actual)
	} //if

	if expected, actual := 0, c.Champion; actual != expected {
		t.Errorf("expected champion %d but was %d", expected, actual)
	} //if

	if expected, actual := 9608, c.Currency; actual != expected {
		t.Errorf("expected currency %d but was %d", expected, actual)
	} //if

	if expected, actual := "@Heppy", c.Account; actual != expected {
		t.Errorf("expected account %s but was %s", expected, actual)
	} //if

	if expected, actual := "EU Megaserver", c.Server; actual != expected {
		t.Errorf("expected server %s but was %s", expected, actual)
	} //if
} //func
