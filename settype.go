package teso

import "strconv"

type SetType int

const (
	SetNone SetType = iota
	SetMonster
	SetZone
	SetDungeon
	SetCraftable
)

func (s SetType) String() string {
	switch s {
	case SetNone:
		return "None"
	case SetMonster:
		return "Monster"
	case SetZone:
		return "Zone"
	case SetDungeon:
		return "Dungeon"
	case SetCraftable:
		return "Craftable"
	default:
		return ""
	} //switch
} //func

func (s *SetType) UnmarshalText(t string) error {
	i, err := strconv.Atoi(t)

	if err != nil {
		return err
	} //if

	*s = SetType(i)

	return nil
} //func
