package teso

import (
	"errors"
	"strconv"
	"strings"
)

const (
	charIDIdx          = 0
	charNameIdx        = 1
	charClassIDIdx     = 2
	charLevelIdx       = 4
	charChampionIdx    = 5
	charRaceIDIdx      = 6
	charAllianceIDIdx  = 8
	charCurrencyIdx    = 12
	charServerIdx      = 14
	charAccountIdx     = 15
	charSkillPointsIdx = 21
	charSkyshardsIdx   = 22
)

// Character is a playable character from ESO.
type Character struct {
	ID         string
	Name       string
	ClassID    string
	AllianceID Alliance
	RaceID     string
	Level      int
	Champion   int
	Currency   int
	Account    string
	Server     string
} //struct

// UnmarshalText attempt to unmarshal a Character from a line.
func (c *Character) UnmarshalText(s string) error {
	start := strings.Index(s, "CHARACTER:")
	if start == -1 {
		return errors.New("not a character string")
	} //if

	s = strings.TrimSpace(s)[start+10:]

	props := strings.Split(s, ";")

	c.ID = strings.TrimSpace(props[charIDIdx])
	c.Name = strings.TrimSpace(props[charNameIdx])
	c.ClassID = strings.TrimSpace(props[charClassIDIdx])
	// c.AllianceID, _ = strconv.Atoi(props[charAllianceIDIdx]) // Ignore error
	c.AllianceID.UnmarshalText(props[charAllianceIDIdx]) // Ignore error
	c.RaceID = strings.TrimSpace(props[charRaceIDIdx])
	c.Level, _ = strconv.Atoi(props[charLevelIdx])       // Ignore error
	c.Champion, _ = strconv.Atoi(props[charChampionIdx]) // Ignore error
	c.Currency, _ = strconv.Atoi(props[charCurrencyIdx]) // Ignore error
	c.Account = strings.TrimSpace(props[charAccountIdx])
	c.Server = strings.TrimSpace(props[charServerIdx])

	return nil
} //func
