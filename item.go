package teso

import (
	"errors"
	"strconv"
	"strings"
)

const (
	itemIDIdx          = 0
	itemNameIdx        = 1
	itemTraitIdx       = 2
	itemEquipTypeIdx   = 3
	itemSetIdx         = 4
	itemQualityIdx     = 5
	itemArmorTypeIdx   = 6
	itemEnchantIdx     = 8
	itemTypeIdx        = 10
	itemChampionIdx    = 11
	itemLevelIdx       = 12
	itemWeaponTypeIdx  = 13
	itemCharacterIdx   = 14
	itemBagTypeIdx     = 15
	itemJunkIdx        = 18
	itemEnchantDescIdx = 20
	itemTraitDescIdx   = 21
	itemValueIdx       = 22
)

// Item is something a character can pick up in ESO
type Item struct {
	ID          string
	Name        string
	Level       int
	Champion    int
	Set         string
	Trait       TraitType
	TraitDesc   string
	Quality     Quality
	Type        ItemType
	EquipType   EquipType
	ArmorType   ArmorType
	WeaponType  WeaponType
	Enchant     string
	EnchantDesc string
	Value       int
	CharacterID string
	BagType     BagType
	IsJunk      bool
} //struct

// UnmarshalText will attempt to unmarshal the item from the provided string.
func (i *Item) UnmarshalText(s string) error {
	start := strings.Index(s, "ITEM:")
	if start == -1 {
		return errors.New("not an item string")
	} //if

	s = strings.ReplaceAll(s, `",`, "")[start+5:]

	props := strings.Split(s, ";")

	if len(props) < 23 {
		return errors.New("malformed item line")
	} //if

	i.ID = strings.TrimSpace(props[itemIDIdx])
	i.Name = strings.TrimSpace(props[itemNameIdx])
	// i.Trait, _ = strconv.Atoi(props[itemTraitIdx]) // Ignore the error here
	i.Trait.UnmarshalTexxt(props[itemTraitIdx])
	i.Level, _ = strconv.Atoi(props[itemLevelIdx])       // Ignore the error here
	i.Champion, _ = strconv.Atoi(props[itemChampionIdx]) // Ignore the error here
	i.Set = strings.TrimSpace(props[itemSetIdx])
	i.TraitDesc = strings.TrimSpace(props[itemTraitDescIdx])
	// i.Quality, _ = strconv.Atoi(props[itemQualityIdx]) // Ignore the error here
	i.Quality.UnmarshalText(props[itemQualityIdx]) // Ignore the error here
	// i.Type, _ = strconv.Atoi(props[itemTypeIdx]) // Ignore the error here
	i.Type.UnmarshalText(props[itemTypeIdx])
	// i.EquipType, _ = strconv.Atoi(props[itemEquipTypeIdx])   // Ignore the error here
	i.EquipType.UnmarshalText(props[itemEquipTypeIdx]) // Ignore the error here
	// i.ArmorType, _ = strconv.Atoi(props[itemArmorTypeIdx])   // Ignore the error here
	i.ArmorType.UnmarshalText(props[itemArmorTypeIdx])
	// i.WeaponType, _ = strconv.Atoi(props[itemWeaponTypeIdx]) // Ignore the error here
	i.WeaponType.UnmarshalText(props[itemWeaponTypeIdx]) //Ignore the error here
	i.Enchant = strings.TrimSpace(props[itemEnchantIdx])
	i.EnchantDesc = strings.TrimSpace(props[itemEnchantDescIdx])
	i.Value, _ = strconv.Atoi(props[itemValueIdx]) // Ignore the error here
	// i.BagType, _ = strconv.Atoi(props[itemBagTypeIdx])     // Ignore the error here
	i.BagType.UnmarshalText(props[itemBagTypeIdx]) // Ignore the error here
	i.CharacterID = strings.TrimSpace(props[itemCharacterIdx])
	i.IsJunk = props[itemJunkIdx] == "true"

	return nil
} //func
