package teso

import (
	"fmt"
	"strconv"
)

// BagType presents different item storage locations is ESO.
type BagType int

// Different bag types.
const (
	BagWorn BagType = iota
	BagBackpack
	BagBank
	BagGuildBank
	BagBuyBack
	BagVirtual
	BagTransfer
	BagHouseBank1
	BagHouseBank2
	BagHouseBank3
	BagHouseBank4
	BagHouseBank5
	BagHouseBank6
	BagHouseBank7
	BagHouseBank8
	BagHouseBank9
	BagHouseBank10
	BagDelete BagType = 255
)

// String is the representation of the BagType.
func (b BagType) String() string {
	switch b {
	case BagWorn:
		return "Worn"
	case BagBackpack:
		return "Backpack"
	case BagBank:
		return "Bank"
	case BagGuildBank:
		return "Guild Bank"
	case BagBuyBack:
		return "Buy Back"
	case BagVirtual:
		return "Virtual"
	case BagTransfer:
		return "Transfer"
	case BagHouseBank1:
		return "Bag House Bank 1"
	case BagHouseBank2:
		return "Bag House Bank 2"
	case BagHouseBank3:
		return "Bag House Bank 3"
	case BagHouseBank4:
		return "Bag House Bank 4"
	case BagHouseBank5:
		return "Bag House Bank 5"
	case BagHouseBank6:
		return "Bag House Bank 6"
	case BagHouseBank7:
		return "Bag House Bank 7"
	case BagHouseBank8:
		return "Bag House Bank 8"
	case BagHouseBank9:
		return "Bag House Bank 9"
	case BagHouseBank10:
		return "Bag House Bank 10"
	case BagDelete:
		return "Delete"
	default:
		return fmt.Sprintf("%d", b)
	} //switch
} //func

// UnmarshalText attempts to set a BagType from the provided string.
func (b *BagType) UnmarshalText(s string) error {
	i, err := strconv.Atoi(s)

	if err != nil {
		return err
	} //if

	*b = BagType(i)

	return nil
} //func
