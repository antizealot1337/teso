package teso

import "strconv"

type WeaponType int

const (
	WeaponNone WeaponType = iota
	WeaponAxe
	WeaponHammer
	WeaponSword
	Weapon2HSword
	Weapon2HAxe
	Weapon2HHammer
	weaponUnused1
	WeaponBow
	WeaponHealingStaff
	weaponUnused2
	WeaponDagger
	WeaponFireStaff
	WeaponFrostStaff
	WeaponShield
	WeaponLightningStaff
)

func (w WeaponType) String() string {
	switch w {
	case WeaponNone:
		return "None"
	case WeaponAxe:
		return "Axe"
	case WeaponHammer:
		return "Hammer"
	case WeaponSword:
		return "Sword"
	case Weapon2HSword:
		return "2h Sword"
	case Weapon2HAxe:
		return "2h Axe"
	case Weapon2HHammer:
		return "2h Hammer"
	case weaponUnused1, weaponUnused2:
		return "unknown"
	case WeaponBow:
		return "Bow"
	case WeaponHealingStaff:
		return "Healing Staff"
	case WeaponDagger:
		return "Dagger"
	case WeaponFireStaff:
		return "Fire Staff"
	case WeaponFrostStaff:
		return "Frost Staff"
	case WeaponShield:
		return "Shield"
	case WeaponLightningStaff:
		return "Lightning Staff"
	default:
		return ""
	} //switch
} //func

func (w *WeaponType) UnmarshalText(s string) error {
	i, err := strconv.Atoi(s)

	if err != nil {
		return err
	} //if

	*w = WeaponType(i)

	return nil
} //func
