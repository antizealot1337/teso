package teso

import "errors"

// Alliance represents the alliance in ESO a Character belongs to.
type Alliance int

// Alliance values
const (
	AllianceNone Alliance = iota
	AllianceAD
	AllianceEP
	AllianceDC
)

// String is the string representation of an Alliance.
func (a Alliance) String() string {
	switch a {
	case AllianceNone:
		return "None"
	case AllianceAD:
		return "AD"
	case AllianceEP:
		return "EP"
	case AllianceDC:
		return "DC"
	} //switch

	return ""
} //func

// UnmarshalText will attempt to set an Alliance based on the provided string.
func (a *Alliance) UnmarshalText(s string) error {
	switch s {
	case "1":
		*a = AllianceAD
		return nil
	case "2":
		*a = AllianceEP
		return nil
	case "3":
		*a = AllianceDC
		return nil
	} //switch

	return errors.New("invalid alliance value")
} //func
