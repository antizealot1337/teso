package teso

import (
	"testing"
)

func TestItemUnmarshalText(t *testing.T) {
	const line = `["BAG-14"] = "ITEM:3.9623432159432;Bogdan the Nightflame's Epaulets;18;4;Nightflame;4;1;true;Maximum Magicka Enchantment;/esoui/art/icons/gear_undaunted_titan_shoulders_a.dds;2;160;50;0;8798292061724666;1;true;1;false;|H0:item:59595:363:50:0:0:0:0:0:0:0:0:0:0:0:1:0:0:1:0:10000:0|h|h;Adds |cffffff324|r Maximum Magicka.;Increases Mundus Stone effects by |cffffff6.5|r%.;1179;14;300;0;en",`

	var i Item

	if err := i.UnmarshalText(line); err != nil {
		t.Fatal("unexpected error:", err)
	} //if

	if expected, actual := "3.9623432159432", i.ID; actual != expected {
		t.Errorf("wrong id\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := "Bogdan the Nightflame's Epaulets", i.Name; actual != expected {
		t.Errorf("wrong name\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := TraitDivines, i.Trait; actual != expected {
		t.Errorf("expected trait %v but was %v", expected, actual)
	} //if

	if expected, actual := 50, i.Level; actual != expected {
		t.Errorf("expected level %d but was %d", expected, actual)
	} //if

	if expected, actual := 160, i.Champion; actual != expected {
		t.Errorf("expected champion level %d but was %d", expected, actual)
	} //if

	if expected, actual := "Nightflame", i.Set; actual != expected {
		t.Errorf("wrong set\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := "Increases Mundus Stone effects by |cffffff6.5|r%.", i.TraitDesc; actual != expected {
		t.Errorf("wrong trait desc\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := QualityLegendary, i.Quality; actual != expected {
		t.Errorf("expected quality %v but was %v", expected, actual)
	} //if

	if expected, actual := ItemArmor, i.Type; actual != expected {
		t.Errorf("expected type %v but was %v", expected, actual)
	} //if

	if expected, actual := EquipShoulders, i.EquipType; actual != expected {
		t.Errorf("expected equip type %v but was %v", expected, actual)
	} //if

	if expected, actual := ArmorLight, i.ArmorType; actual != expected {
		t.Errorf("expected armor type %v but was %v", expected, actual)
	} //if

	if expected, actual := WeaponNone, i.WeaponType; actual != expected {
		t.Errorf("expected weapon type %v but was %v", expected, actual)
	} //if

	if expected, actual := "Maximum Magicka Enchantment", i.Enchant; actual != expected {
		t.Errorf("wrong enchantment\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := "Adds |cffffff324|r Maximum Magicka.", i.EnchantDesc; actual != expected {
		t.Errorf("wrong enchantment description\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := 1179, i.Value; actual != expected {
		t.Errorf("expected value %d but was %d", expected, actual)
	} //if

	if expected, actual := "8798292061724666", i.CharacterID; actual != expected {
		t.Errorf("wrong character id\n\t\twant:\"%s\"\n\t\t got:\"%s\"", expected, actual)
	} //if

	if expected, actual := BagBackpack, i.BagType; actual != expected {
		t.Errorf("expected bag type %v but was %v", expected, actual)
	} //if
} //func
