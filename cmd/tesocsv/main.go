package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/antizealot1337/teso"
)

var (
	fltr   filter
	output = "out.csv"
)

func init() {
	// Setup the command line flags
	flag.StringVar(&output, "out", output,
		"The file to write the data to or STDOUT if \"--\" is supplied")
	flag.BoolVar(&fltr.armor, "armor", fltr.armor, "Output armor")
	flag.BoolVar(&fltr.weapons, "weapons", fltr.weapons, "Output weapons")
	flag.BoolVar(&fltr.motifs, "motifs", fltr.motifs, "Output motifs")
	flag.BoolVar(&fltr.recipes, "recipes", fltr.recipes, "Output recipes")
	flag.BoolVar(&fltr.jewelry, "jewelry", fltr.jewelry, "Output jewelry")
	flag.BoolVar(&fltr.misc, "misc", fltr.misc, "Output miscellaneous items")
	all := flag.Bool("all", false, "Output everything (deprecated)")
	gear := flag.Bool("gear", false, "Outputs armor, weapons, and jewelry")

	// Setup the usage
	flag.Usage = func() {
		out := flag.CommandLine.Output()

		fmt.Fprintln(out, "Usage:", filepath.Clean(os.Args[0]), "<TESO DELVE FILE>")

		fmt.Fprintln(out, "Flags:")
		flag.PrintDefaults()
	} //func

	// Parse the command line flags
	flag.Parse()

	// Use all to update other flags
	fltr.armor = fltr.armor || *gear || *all
	fltr.weapons = fltr.weapons || *gear || *all
	fltr.jewelry = fltr.jewelry || *gear || *all
	fltr.motifs = fltr.motifs || *all
	fltr.recipes = fltr.recipes || *all
	fltr.misc = fltr.misc || *all
} //func

func main() {
	if flag.NArg() == 0 {
		fmt.Fprintln(os.Stderr, "error: no file provided")
		os.Exit(2)
	} //if

	if !fltr.hasFilter() {
		fltr.setAll(true)
	} //if

	characters, items, err := readTesoFile(flag.Arg(0))

	if err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	} //if

	err = writeCSVFile(output, fltr, characters, items)

	if err != nil {
		fmt.Fprintln(os.Stderr, "error:", err)
		os.Exit(1)
	} //if
} //func

func readTesoFile(path string) (map[string]*teso.Character, []*teso.Item, error) {
	f, err := os.Open(path)

	if err != nil {
		return nil, nil, err
	} //if

	defer f.Close()

	scanner := bufio.NewScanner(f)
	characters := map[string]*teso.Character{}
	items := []*teso.Item{}

	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())

		switch {
		case strings.Contains(line, "CHARACTER:"):
			var c teso.Character

			err := c.UnmarshalText(line)

			if err != nil {
				fmt.Fprintln(os.Stderr, "error:", err)
				continue
			} //if

			characters[c.ID] = &c
		case strings.Contains(line, "ITEM:"):
			var i teso.Item

			err := i.UnmarshalText(line)

			if err != nil {
				fmt.Fprintln(os.Stderr, "error:", err)
				continue
			} //if

			items = append(items, &i)
		} //switch
	} //for

	return characters, items, scanner.Err()
} //func

func writeCSVFile(path string, fltr filter,
	characters map[string]*teso.Character,
	items []*teso.Item) (err error) {

	var f io.WriteCloser
	f, err = openOutput(path)

	if err != nil {
		return
	} //if

	defer func() {
		// Check for an error
		if err != nil {
			// Ignore close errors
			f.Close()
		} //if

		// Close the file
		err = f.Close()
	}() //func

	for _, item := range items {
		if !fltr.matches(item) {
			continue
		} //if

		level := itemLevel(item)
		location := itemLoc(item, characters)

		fmt.Fprintf(f, "%s,%s,%s,%s,%v,%s,%s,%s,\n", item.Name, level,
			item.Quality, item.EquipType, item.BagType == teso.BagWorn,
			item.Set, item.Trait, location)
	} //for

	return nil
} //func

func openOutput(path string) (io.WriteCloser, error) {
	if path == "--" || path == "" {
		return &nopWriter{w: os.Stdout}, nil
	} //if

	f, err := os.Create(path)

	if err != nil {
		return nil, err
	} //if

	return f, nil
} //func

func itemLevel(i *teso.Item) string {
	if i.Champion > 0 {
		return fmt.Sprintf("50+%d", i.Champion)
	} //if

	return fmt.Sprintf("%d", i.Level)
} //func

func itemLoc(i *teso.Item, chars map[string]*teso.Character) string {
	switch i.BagType {
	case teso.BagBackpack, teso.BagWorn:
		c, ok := chars[i.CharacterID]

		if !ok {
			return i.BagType.String()
		} //if

		return c.Name
	default:
		return i.BagType.String()
	} //switch
} //func
