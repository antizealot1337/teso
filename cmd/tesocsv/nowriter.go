package main

import "io"

type nopWriter struct {
	w io.Writer
} //struct

func (n *nopWriter) Write(b []byte) (int, error) {
	return n.w.Write(b)
} //func

func (n *nopWriter) Close() error {
	return nil
} //func
