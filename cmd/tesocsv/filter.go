package main

import "bitbucket.org/antizealot1337/teso"

type filter struct {
	armor   bool
	weapons bool
	jewelry bool
	motifs  bool
	recipes bool
	misc    bool
} //struct

func (f *filter) matches(i *teso.Item) bool {
	switch i.Type {
	case teso.ItemArmor:
		switch i.EquipType {
		case teso.EquipRing, teso.EquipNeck:
			return f.jewelry
		default:
			return f.armor
		} //switch
	case teso.ItemWeapon:
		return f.weapons
	case teso.ItemRacialStyleMotif:
		return f.motifs
	case teso.ItemRecipe:
		return f.recipes
	default:
		return f.misc
	} //switch
} //func

func (f *filter) hasFilter() bool {
	return f.armor || f.weapons || f.jewelry || f.motifs || f.recipes || f.misc
} //func

func (f *filter) setAll(enable bool) {
	f.armor = enable
	f.weapons = enable
	f.jewelry = enable
	f.motifs = enable
	f.recipes = enable
	f.misc = enable
} //func
